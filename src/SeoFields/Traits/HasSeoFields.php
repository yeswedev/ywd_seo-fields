<?php
namespace YesWeDev\SeoFields\Traits;

trait HasSeoFields
{
    public function extraConstructHasSEOFields() : void
    {
        $this->fillable = array_merge($this->fillable, ['meta_title','meta_description','h1']);
        if($this->translatedAttributes) {
            $this->translatedAttributes = array_merge( $this->translatedAttributes, ['meta_title','meta_description','h1']);
        }
    }

    public function getMetaTitle(Array $arr = null, $param = null) : String
    {
        $metaTitle;
        settype($metaTitle,'string');

        $arrayOrString = trans('meta.title.'.$this->getTable());

        if($this->meta_title)
        {
            $metaTitle = $this->meta_title;
        }
        elseif($arr)
        {
            if($param == 'wellness')
            {
                $metaTitle = trans('meta.title.club-details-wellness', $arr);
            }
            else
            {
                $metaTitle = trans('meta.title.'.$this->getTable(), $arr);
            }
        }
        elseif($this->url)
        {
            if(array_key_exists($this->url, $arrayOrString))
            {
                $metaTitle = $arrayOrString[$this->url];
            }
            elseif(array_key_exists('default', $arrayOrString))
            {
                $metaTitle = $arrayOrString['default'];
            }
        }
        else
        {
            $metaTitle = '';
        }

        return $metaTitle;
    }

    public function getMetaDescription(Array $arr = null, $param = null) : String
    {
        $metaDescription;
        settype($metaDescription,'string');

        $arrayOrString = trans('meta.description.'.$this->getTable());

        if($this->meta_description)
        {
            $metaDescription = $this->meta_description;
        }
        elseif($arr)
        {
            if($param == 'wellness')
            {
                $metaDescription = trans('meta.description.club-details-wellness', $arr);
            }
            else
            {
                $metaDescription = trans('meta.description.'.$this->getTable(), $arr);
            }
        }
        elseif($this->url)
        {
            if(array_key_exists($this->url, $arrayOrString))
            {
                $metaDescription = $arrayOrString[$this->url];
            }
            elseif(array_key_exists('default', $arrayOrString))
            {
                $metaDescription = $arrayOrString['default'];
            }
        }
        else
        {
            $metaDescription = '';
        }

        return $metaDescription;
    }

    public function getH1($param = null) : String
    {
        $h1;
        settype($h1,'string');

        if($this->h1)
        {
            $h1 = $this->h1;
        }
        elseif($this->title && $param == null)
        {
            $h1 = $this->title;
        }
        else
        {
            $h1 = '';
        }

        return $h1;
    }
}
